'use strict';

const APP_WIDTH = 1920;
const APP_HEIGHT = 1080;
const APP_URL = 'http://dimin.com';

const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

let win;

function createWindow() {
  win = new BrowserWindow({
    width: APP_WIDTH,
    height: APP_HEIGHT,
    fullscreen: true,
    autoHideMenuBar: true,
    frame: false,
    kiosk: true,
    x: 0,
    y: 0,
    alwaysOnTop: true,
    webPreferences: {
      nodeIntegration: false
    }
  });

  win.loadURL(APP_URL);

  win.on('closed', function() {
    win = null;
  });

  win.on('blur', function() {
    win.setAlwaysOnTop(true);
    win.setFullScreen(true);
    win.show();
    win.focus();
  });

}

app.on('ready', createWindow);

app.on('window-all-closed', function() {
  app.quit();
});
