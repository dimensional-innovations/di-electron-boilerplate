# DI Electron Boilerplate

Use this to create new kiosk applications. This will only work for applications where the content and front-end code are being served by a separate web server.

1. To get started, [download this repo as a ZIP file.](https://gitlab.com/dimensional-innovations/di-electron-boilerplate/repository/archive.zip?ref=master)

2. Next, download the most recent version of [Electron](https://github.com/atom/electron/releases). The link you'll be looking for will look like `electron-v0.XX.X-win32-x64.zip` where `v0.XX.X` is the most recent version available.

3. Unzip that file and place the `app` folder from this repot inside the `resources` folder.

4. Next, edit lines 3, 4 and 5 of `app/index.js` to suit your project. There are three variables you will update.

   `APP_WIDTH` - The width in pixels of the application.

   `APP_HEIGHT` - The height in pixels of the application.

   `APP_URL` - The url where the application is being served.

5. **Changing the application icon.** The executable file named `electron.exe` can be renamed anything you want. No spaces or special characters, please. Right click on this executable file and choose `Create Shortcut` and drag this new shortcut to the desktop. To change the icon of this shortcut, right click and select `Properties`. Go to the `shortcut` tab and click `Change Icon`. Browse to the icon file in this repo: `icon/DI.ico`. Finally rename this shortcut as needed.
